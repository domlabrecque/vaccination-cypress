
/**
 * @class
 * @alias module:identity
 * @property {String} Scenario
 * @property {String} LastName
 * @property {String} FirstName
 * @property {String} BirthDate
 * @property {String} PostalCode
 * @property {String} Gender
 * @property {String} HealthInsuranceNumber
 * @property {String} MotherFirstName
 * @property {String} MotherMaidenName
 * @property {String} FatherFirstName
 * @property {String} FatherLastName
 * @property {String} FirstVaccinationDate
 * @property {String} VaccineManufacturer
 * @property {String} ExpectedResponseCode
 */
