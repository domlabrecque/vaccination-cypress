Feature: Navigation

    Scenario Outline: Url with <endpoint> should display the form in <language>
        When    I navigate to endpoint <endpoint>
        Then    The form should be displayed in <language>

        Examples:
            | endpoint | language |
            | /en      | english  |
            | /fr      | french   |