Feature: Proof of Vaccination:

  As a user, I want to be able to download my proof of vaccination

  Background:
    Given   I navigate to /PreuveVaccinale
    And     I use data from scenarios_20210708


  Scenario Outline: Succès - <scenario>
    Given   I switch language to <language>
    And     I use identity scenario: <scenario>
    When    I fill the form
    And     I submit my identity information
    Then    I should be redirected to a confirmation page
    And     I should be able to download my proof of vaccination

    Examples:
      | language | scenario                                                                 |
      | french   | A deux doses de vaccin Covid valides                                     |
      | english  | A recu 2 doses Covid, la première est invalide et la deuxième est valide |
      | french   | A recu une dose valide de Covid                                          |
      | english  | Foetus a reçu sa 1ère dose avant sa naissance                            |


  Scenario Outline: Échec - Popup Attention - <scenario>
    Given   I switch language to <language>
    And     I use identity scenario: <scenario>
    When    I fill the form
    And     I submit my identity information
    And     I should see a warning popup stating it is not possible to download my proof

    Examples:
      | language | scenario                                                                                                  |
      | english  | Pas vacciné contre la Covid                                                                               |
      | french   | A une dose de vaccin Covid valide mais n'a pas saisi le bon nom commercial lorsqu'il a demandé sa preuve |
