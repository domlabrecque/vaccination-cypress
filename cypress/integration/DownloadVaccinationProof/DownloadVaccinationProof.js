import { Before, Given, When, Then } from "cypress-cucumber-preprocessor/steps";
import dayjs from "dayjs";

Before(() => {
    cy.task('deleteFolder', Cypress.config('downloadsFolder'));
});


Given(/^I navigate to (.+)$/, (endpoint) => {
    cy.visit(endpoint);
});

Given(/^I use data from (\w+)$/, (fixtureName) => {
    Cypress.env('fixtureName', fixtureName);
})

Given(/^I switch language to (english|french)$/, (expectedLanguage) => {
    expectedLanguage = expectedLanguage.toLowerCase();
    Cypress.env('language', expectedLanguage);
    Cypress.env('fileName', Cypress.env('fileNamePrefix')[expectedLanguage]);
    cy.contains('button', /english|fran.ais/i).then($button => {
        cy.wrap($button, {log: false}).then($button => {
            const buttonLanguage = $button.text().toLowerCase();
            if (buttonLanguage === 'english' && expectedLanguage === 'english') {
                cy.wrap($button).click();
            } else if (buttonLanguage !== 'english' && expectedLanguage !== 'english') {
                cy.wrap($button).click();
            }
        });
    });
});

Given(/^I use identity scenario: (.+)$/, scenario => {
    cy.fixture(Cypress.env('fixtureName')).then(identities => {
        const identity = identities.find(identity => identity.Scenario === scenario)
        cy.wrap(identity).should('not.be.undefined').then(() => {

            const firstName = Cypress._.capitalize(identity.FirstName);
            const lastName = Cypress._.capitalize(identity.LastName);
            const dateStamp = dayjs().format('YYYYMMDD');
            Cypress.env('identity', identity);
            Cypress.env('fileName', `${Cypress.env('fileName')}_${firstName}_${lastName}_${dateStamp}.pdf`);
            cy.log('Scenario', {identities, identity})
        });
    });
})


When('I fill the form', () => {
    const identity = Cypress.env('identity');
    identity.VaccineManufacturer = Cypress.env('vaccineManufacturer')[identity.VaccineManufacturer];
    cy.input('lastName').type(identity.LastName);
    cy.input('firstName').type(identity.FirstName);
    cy.input('birthDate').type(identity.BirthDate).blur();
    if (identity.PostalCode) {
        cy.input('postalCode').type(identity.PostalCode);
    }
    const gender = identity.Gender === 'F' ? '1' : '0';
    cy.input('gender').check(gender);
    if (identity.HealthInsuranceNumber) {
        cy.input('healthInsuranceNumber').type(identity.HealthInsuranceNumber);
        cy.input('isHealthInsuranceNumberUnknown').uncheck();
    } else {
        cy.input('isHealthInsuranceNumberUnknown').check();
    }
    if (identity.MotherMaidenName) {
        cy.input('motherMaidenName').type(identity.MotherMaidenName);
        cy.input('motherFirstName').type(identity.MotherFirstName);
        cy.input('isMotherUnknown').uncheck();
    } else {
        cy.input('isMotherUnknown').check();
    }
    if (identity.FatherLastName) {
        cy.input('fatherLastName').type(identity.FatherLastName);
        cy.input('fatherFirstName').type(identity.FatherFirstName);
        cy.input('isFatherUnknown').uncheck();
    } else {
        cy.input('isFatherUnknown').check();
    }
    cy.input('firstVaccinationDate').type(identity.FirstVaccinationDate);
    cy.get('input[id^="react-select-"][id$="-input"]').click({force: true});
    cy.get('div[id^="react-select-"][id*="-option-"]').contains(identity.VaccineManufacturer).click();
    cy.input('consent').check();
});

When('I submit my identity information', () => {
    cy.contains('button', /submit|soumettre/i).click();
});


Then('I should be redirected to a confirmation page', () => {
    cy.url({timeout:30000}).should('contain', 'token=');
    cy.contains('button[type="submit"]', /t.l.charger|download/i)
        .as('downloadButton')
        .should('be.visible');
});

Then('I should be able to download my proof of vaccination', () => {
    cy.get('@downloadButton').click();
    const { downloadsFolder } = Cypress.config();
    const { fileName } = Cypress.env();
    const separator = /\\/.test(downloadsFolder) ? '\\' : '/';
    const fileNameWithPath = downloadsFolder + separator + fileName;
    cy.readFile(fileNameWithPath, {timeout: 30000}); // used for syncing -> cy.readfile has a timeout option
    cy.task('readPdf', fileNameWithPath).should(pdf => {
        expect(pdf).to.have.property('numpages').greaterThan(0);
    });
});

Then('I should see a warning popup stating it is not possible to download my proof', () => {
    const popupTitle = Cypress.env('language') === 'french' ? 'Attention' : 'Warning';
    cy.get('div.modal-content', {timeout: 30000}).should('be.visible')
        .invoke('text').should('contain', popupTitle);
});
