/// <reference types="cypress" />

declare namespace Cypress {
    interface Chainable {

        /**
         * Custom command to get any element from a list
         * @example cy.get('div').any().click(); // clicks 1 random div
         * @example cy.get('div').any(3).click({multiple: true}); // clicks 3 random divs
         */
        input(name: string, options?: Partial<Loggable & Timeoutable & Withinable & Shadow>): Chainable<Element>;

    }
}
